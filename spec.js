var helper = require('./helper');

describe('Protractor Demo App', function() {
  // it('should have a title', function() {
  //   browser.get('http://juliemr.github.io/protractor-demo/');
  //
  //   expect(browser.getTitle()).toEqual('Super Calculator');
  // });

  it('should have a title', function() {
    browser.get(browser.params.url);
    var logInLink = $('._fcn8k');
    var loginButton = $('._ah57t._84y62._i46jh._rmr7s');
    var usernameField = $('._kp5f7._qy55y');
    var passwordField = $('._kp5f7._1mdqd._qy55y');
    logInLink.click();
    usernameField.sendKeys('Pointnoview');
    passwordField.sendKeys('Pointnoview123');
    // passwordField.sendKeys(protractor.Key.ENTER);
    loginButton.click();
    helper.waitUntilReady(element(by.css('._a1rcs')));
    element(by.css('._a1rcs')).click();
    expect(browser.getTitle()).toEqual('Instagram');
  });
  // it('should have a title', function() {
  //   browser.get('https://www.mail.ru');
  //
  //   expect(browser.getTitle()).toEqual('Mail.Ru: почта, поиск в интернете, новости, игры');
  // });

});
