exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['spec.js'],
  params: {
    url: 'https://www.instagram.com/'
  },
  onPrepare: function() {
    browser.ignoreSynchronization = true;
  }
}
